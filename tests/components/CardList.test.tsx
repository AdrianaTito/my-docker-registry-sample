import React from 'react';
import { shallow } from 'enzyme';
import CardList from '../../src/components/CardList';

const workspaces = [
    {
        id: '91894f49-c77e-4a81-8d41-84b160186726',
        name: 'Dev-Bootcamp06-LP',
        description: 'This workspace is to share information between 06 bootcampers and the Sr. Lead'
    },
    {
        id: '8d2329b9-f2fc-4f50-88ca-1cdfbe5fc50c',
        name: 'Dev-Bootcamp05-LP',
        description: 'This workspace is to share information between 05 bootcampers and the Sr. Lead'
    }
];

describe('In CardList component', () => {
    const props = { workspaces };
    const cardList = shallow(<CardList {...props}/>);
    it('should render successfully', () => {
        expect(CardList).toMatchSnapshot();
    });
});
