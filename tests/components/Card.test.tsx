import React from 'react';
import { shallow } from 'enzyme';
import SimpleCard from '../../src/components/Card';

const workspace = {
    id: '91894f49-c77e-4a81-8d41-84b160186726',
    name: 'Dev-Bootcamp06-LP',
    description: 'This workspace is to share information between 06 bootcampers and the Sr. Lead'
};

describe('In CardComponent', () => {
    const props = { ...workspace };
    const Card = shallow(<SimpleCard {...props}/>);
    it('should render successfully', () => {
        expect(Card).toMatchSnapshot();
    });
});
