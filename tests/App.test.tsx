import React from 'react';
import App, { IAppState } from '../src/App';
import { shallow, mount } from 'enzyme';
import { getWorkspaces } from '../src/Api/getApi';

const app = shallow<App, {}, IAppState>(<App/>);

describe('in App', () => {
    it('should render correctly', () => {
        expect(app).toMatchSnapshot();
    });

    it('should initializes "state" with an empty list of workspaces', () => {
        expect(app.state().workspaces).toEqual([]);
    });
});
