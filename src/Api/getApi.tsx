import { IWorkspace } from '../definitions/IWorkspace';
import { urlApi } from './apiConfig';

export const getWorkspaces = async () :Promise<IWorkspace[]> => {
    console.log(urlApi);
    const response = await window.fetch(urlApi);
    const workspaces = await response.json();
    return workspaces;
};
