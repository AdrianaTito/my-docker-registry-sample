import React from 'react';
import './App.scss';
import { IWorkspace } from './definitions/IWorkspace';
import { getWorkspaces } from './Api/getApi';
import Header from './components/Header/';
import WSBoard from './components/WSBoard';
import ErrorLoading from './warnings/ErrorLoading';

interface IAppProps { }
interface IAppState {
    workspaces: IWorkspace[],
    error: boolean
}
declare global {
    interface Window {
        _env_:any;
    }
}

class App extends React.Component <IAppProps, IAppState> {
    constructor (props:IAppProps) {
        super(props);
        this.state = {
            workspaces: [],
            error: false
        };
    }

    async componentDidMount () {
        try {
            const workspaces = await getWorkspaces();
            this.setState({ workspaces });
        } catch (error) {
            this.setState({ error: true });
        }
    }

    render () {
        return (
            <div className="App">
                <Header/>
                <h3>API_URL: {window._env_.API_URL}</h3>
                { this.state.error ? <ErrorLoading/>
                    : <WSBoard workspaces={this.state.workspaces}/> }
            </div>
        );
    }
}

// eslint-disable-next-line no-undef
export type { IAppState };
export default App;
