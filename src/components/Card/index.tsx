import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

interface ISimpleCardProps{
    id: string,
    name: string,
    description: string
}

export default function SimpleCard ({ id, name, description }:ISimpleCardProps) {
    return (
        <Card >
            <CardContent>
                <div className='card-header'>
                    <Typography variant='h4' className='demo-title'>
                        {name}
                    </Typography>
                </div>
                <hr/>
                <Typography variant="subtitle2" color="textPrimary" className='ws-description'>
                    {description}
                </Typography>
            </CardContent>
        </Card>
    );
}
