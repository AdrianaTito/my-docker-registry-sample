import React from 'react';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import './SearchBar.scss';
import logo from '../../assets/logo.png';
import InputAdornment from '@material-ui/core/InputAdornment';

export default function SearchBar () {
    return (
        <div className="search-container">
            <div >
                <img src= {logo} className="logo" alt="logo"></img>
            </div>
            <div className="search-box">
                <div>
                    <TextField
                        id="outlined"
                        variant="outlined"
                        placeholder="Search"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <SearchIcon />
                                </InputAdornment>
                            )
                        }}
                    />
                </div>
            </div>
        </div>
    );
}
