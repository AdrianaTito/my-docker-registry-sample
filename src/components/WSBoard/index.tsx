import React from 'react';
import './WSBoard.scss';
import FolderIcon from '@material-ui/icons/Folder';
import Typography from '@material-ui/core/Typography';
import CardList from '../CardList';
import { IWorkspace } from '../../definitions/IWorkspace';

interface ICardListProps{
    workspaces: IWorkspace[]
}

export default function WSBoard ({ workspaces }: ICardListProps) {
    return (
        <div className="wsbody-container" >
            <div className="ws-title-container">
                <div className="ws-title-box">
                    <div className="box-foldericon">
                        <FolderIcon fontSize="inherit" className="folder-icon"/>
                    </div>
                    <div className="ws-title">
                        <Typography align='center' variant='h5' color='textSecondary' >
                            Worskpaces
                        </Typography>
                    </div>
                </div>
            </div>
            <div className="ws-board">
                <div className="ws-cards">
                    <CardList workspaces={workspaces}/>
                </div>
            </div>
        </div>
    );
}
