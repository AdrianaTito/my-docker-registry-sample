import React from 'react';
import SimpleCard from '../Card';
import { IWorkspace } from '../../definitions/IWorkspace';
import './CardList.scss';

interface ICardListProps{
    workspaces: IWorkspace[]
}

export default function CardList ({ workspaces }: ICardListProps) {
    return (
        <div className="box-cardlist">
            {
                workspaces.map((workspace: IWorkspace) => {
                    return (
                        <div className="cards" key={ workspace.id}>
                            <SimpleCard
                                id={workspace.id}
                                name={ workspace.name}
                                description={ workspace.description}
                            />
                        </div>
                    );
                })
            }
        </div>
    );
}
