import React from 'react';
import './Header.scss';
import SearchBar from '../SearchBar';

export default function Header () {
    return (
        <div className="header-container" >
            <SearchBar/>
        </div>
    );
}
