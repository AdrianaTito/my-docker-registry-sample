import React from 'react';

export default function ErrorLoading () {
    return (
        <div>
            <h2>Error while loading the workspaces</h2>
        </div>
    );
}
