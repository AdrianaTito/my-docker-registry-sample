


#RUN npm rebuild node-sass
#RUN npm run build

FROM nginx:alpine
COPY ./ ./
WORKDIR /app
COPY nginx.conf /etc/nginx/nginx.conf
COPY /dist /app/dist 
COPY /dist /usr/share/nginx/html/
EXPOSE 80

WORKDIR /usr/share/nginx/html
COPY .env ./
COPY ./env.sh ./
# Add bash
RUN apk add --no-cache bash
# Make our shell script executable
RUN chmod +x env.sh

ENTRYPOINT ["/bin/bash", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]